# Transfer Service

Provides simple REST API for transferring money between accounts. The API focusses
mainly on creating standing orders for transfers.

To run the tests:

```
mvn test
```

To build the project:

```
mvn compile install
```

To run the project:

```
java -jar target/transfer-service-1.0-SNAPSHOT.jar
```


## API calls

To test the creation of transfer order

```
curl -X POST \
  http://127.0.0.1:8080/api/transfer-orders \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
  "sender_id": "01924c48-49bb-40c2-9c32-ab582e6db6f4",
  "recipient_account_number": 12345,
  "recipient_bank_code": "ABC12345",
  "amount": 10000,
  "scheduled_at": "2019-07-21T17:32:28Z"
}'
```

To test the retrieval of transfer order

```
curl -X GET \
  http://localhost:8080/api/transfer-orders/<ORDER-UUID>
```

## NOTE

Order processing is not included in the current version. 