package com.revolut.transferservice;

import com.revolut.transferservice.server.DatabaseServer;
import com.revolut.transferservice.server.UndertowServer;

public class Main {
    public static void main(final String[] args) throws Exception {

        final DatabaseServer dbServer = new DatabaseServer(ApplicationProperties.DB_NAME, ApplicationProperties.DB_USER,
                ApplicationProperties.DB_PASSWORD);
        dbServer.start();

        final UndertowServer server = new UndertowServer();
        server.start();
    }
}
