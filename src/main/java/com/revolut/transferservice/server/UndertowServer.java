package com.revolut.transferservice.server;

import static com.revolut.transferservice.ApplicationProperties.APPLICATION_NAME;
import static com.revolut.transferservice.ApplicationProperties.APP_HOST;
import static com.revolut.transferservice.ApplicationProperties.APP_PORT;
import static com.revolut.transferservice.ApplicationProperties.CONTEXT_PATH;
import static com.revolut.transferservice.ApplicationProperties.IO_THREADS;
import static com.revolut.transferservice.ApplicationProperties.ROOT_PATH;
import static com.revolut.transferservice.ApplicationProperties.WORKER_THREADS;

import org.jboss.logging.Logger;

import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;

import com.revolut.transferservice.Main;
import com.revolut.transferservice.TransferApplication;

import io.undertow.Handlers;
import io.undertow.Undertow;

import io.undertow.server.handlers.resource.ClassPathResourceManager;

import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;

public class UndertowServer {

    private static final Logger LOGGER = Logger.getLogger(UndertowServer.class);

    private UndertowJaxrsServer server;

    public UndertowServer() {
        server = new UndertowJaxrsServer();
    }

    public void start() {
        server.deploy(getDeploymentInfo(getResteasyDeployment()));

        server.addResourcePrefixPath(ROOT_PATH,
            Handlers.resource(new ClassPathResourceManager(Main.class.getClassLoader())));

        server.start(getBuilder());

        LOGGER.info("Undertow server ready to accept requests!");
    }

    private Undertow.Builder getBuilder() {

        return Undertow.builder().addHttpListener(APP_PORT, APP_HOST).setIoThreads(IO_THREADS).setWorkerThreads(
                WORKER_THREADS);
    }

    private ResteasyDeployment getResteasyDeployment() {
        final ResteasyDeployment deployment = new ResteasyDeployment();
        deployment.setApplicationClass(TransferApplication.class.getName());
        deployment.setInjectorFactoryClass("org.jboss.resteasy.cdi.CdiInjectorFactory");

        return deployment;
    }

    private DeploymentInfo getDeploymentInfo(final ResteasyDeployment deployment) {
        final DeploymentInfo deploymentInfo = server.undertowDeployment(deployment, ROOT_PATH);
        deploymentInfo.setClassLoader(Main.class.getClassLoader());
        deploymentInfo.setDeploymentName(APPLICATION_NAME);
        deploymentInfo.setContextPath(CONTEXT_PATH);
        deploymentInfo.addListener(Servlets.listener(org.jboss.weld.environment.servlet.Listener.class));

        return deploymentInfo;
    }
}
