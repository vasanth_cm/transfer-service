package com.revolut.transferservice.server;

import static com.revolut.transferservice.database.queries.Queries.getTransferOrderTableQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.h2.tools.Server;

import org.jboss.logging.Logger;

import com.revolut.transferservice.database.pool.DBConnectionPool;

public class DatabaseServer {

    private static final Logger LOGGER = Logger.getLogger(DatabaseServer.class);

    private String dbName;

    private String user;

    private String password;

    public DatabaseServer(final String dbName, final String user, final String password) throws Exception {
        this.dbName = dbName;
        this.user = user;
        this.password = password;
    }

    public void start() throws SQLException {
        try {
            Server.createTcpServer("-tcpAllowOthers").start();
            Class.forName("org.h2.Driver");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        LOGGER.info(String.format("Started database [%s]", dbName));

        try {
            final Connection connection = DBConnectionPool.getConnection();
            final PreparedStatement statement = connection.prepareStatement(getTransferOrderTableQuery());
            statement.executeUpdate();
            LOGGER.info("Required tables created successfully");
        } catch (final SQLException sqe) {
            LOGGER.error("Failed table creation.", sqe);
            throw sqe;
        }
    }
}
