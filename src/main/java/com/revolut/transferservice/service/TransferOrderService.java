package com.revolut.transferservice.service;

import static com.revolut.transferservice.database.queries.Queries.getTransferOrderCreationQuery;
import static com.revolut.transferservice.database.queries.Queries.getTransferOrderRetrievalQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.UUID;

import org.jboss.logging.Logger;

import com.revolut.transferservice.database.pool.DBConnectionPool;
import com.revolut.transferservice.rest.resources.TransferOrder;

public class TransferOrderService {

    private static final Logger LOGGER = Logger.getLogger(TransferOrderService.class);

    private Connection connection;

    public TransferOrderService() throws Exception {
        connection = DBConnectionPool.getConnection();
    }

    public TransferOrderService(final Connection connection) {
        this.connection = connection;
    }

    public UUID createTransferOrder(final TransferOrder request) {
        final UUID transferOrderId = UUID.randomUUID();

        try {
            final PreparedStatement statement = connection.prepareStatement(getTransferOrderCreationQuery(request,
                        transferOrderId));
            statement.execute();
        } catch (final SQLException sqe) {
            LOGGER.error("Failed creation", sqe);
            return null;
        }

        return transferOrderId;
    }

    public TransferOrder getTransferOrder(final UUID id) {

        try {
            final PreparedStatement statement = connection.prepareStatement(getTransferOrderRetrievalQuery(id));
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final UUID senderId = (UUID) resultSet.getObject("to_sender_id");
                final int recipientAccount = resultSet.getInt("to_recipient_account");
                final String recipientBankCode = resultSet.getString("to_recipient_bank");
                final int amount = resultSet.getInt("to_amount");
                final String scheduledAt = resultSet.getString("to_scheduled_at");

                return new TransferOrder(senderId, recipientAccount, recipientBankCode, amount, scheduledAt, "PENDING");
            }
        } catch (final SQLException sqe) {
            LOGGER.error("Failed fetch for id {}", id, sqe);
            return null;
        }

        LOGGER.warn("No order found for id: " + id);
        return null;
    }
}
