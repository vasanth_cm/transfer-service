package com.revolut.transferservice.rest.resources;

import static org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.ANY;

import java.util.UUID;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonAutoDetect(fieldVisibility = ANY)
public class TransferOrderResponse {

    private UUID transferOrderId;

    @JsonCreator
    public TransferOrderResponse(@JsonProperty("transfer_order_id") final UUID transferOrderId) {
        this.transferOrderId = transferOrderId;
    }

    public void setTransferOrderId(final UUID transferOrderId) {
        this.transferOrderId = transferOrderId;
    }
}
