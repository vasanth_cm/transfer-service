package com.revolut.transferservice.rest.resources;

import static org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.*;

import java.util.UUID;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonAutoDetect(fieldVisibility = ANY)
public class TransferOrder {
    private UUID senderId;

    private int recipientAccountNumber;

    private String recipientBankCode;

    private long amount;

    private String scheduledAt;

    private String status;

    @JsonCreator
    public TransferOrder(@JsonProperty("sender_id") final UUID senderId,
            @JsonProperty("recipient_account_number") final int recipientAccountNumber,
            @JsonProperty("recipient_bank_code") final String recipientBankCode,
            @JsonProperty("amount") final long amount,
            @JsonProperty("scheduled_at") final String scheduledAt,
            @JsonProperty("status") final String status) {
        this.senderId = senderId;
        this.recipientAccountNumber = recipientAccountNumber;
        this.recipientBankCode = recipientBankCode;
        this.amount = amount;
        this.scheduledAt = scheduledAt;
        this.status = status;
    }

    public UUID getSenderId() {
        return senderId;
    }

    public int getRecipientAccountNumber() {
        return recipientAccountNumber;
    }

    public String getRecipientBankCode() {
        return recipientBankCode;
    }

    public long getAmount() {
        return amount;
    }

    public String getScheduledAt() {
        return scheduledAt;
    }

    public String getStatus() {
        return status;
    }
}
