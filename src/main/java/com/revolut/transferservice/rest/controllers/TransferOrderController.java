package com.revolut.transferservice.rest.controllers;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.revolut.transferservice.rest.resources.TransferOrder;
import com.revolut.transferservice.rest.resources.TransferOrderResponse;
import com.revolut.transferservice.service.TransferOrderService;

@Path("/transfer-orders")
public class TransferOrderController {

    private TransferOrderService orderService;

    public TransferOrderController() throws Exception {
        orderService = new TransferOrderService();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTransferOrder(final TransferOrder request) {
        final UUID transferOrderId = orderService.createTransferOrder(request);
        final TransferOrderResponse response = new TransferOrderResponse(transferOrderId);

        return Response.accepted(response).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransferOrder(@PathParam("id") final UUID id) {
        final TransferOrder order = orderService.getTransferOrder(id);

        if (order == null) {
            return Response.status(404).entity("Transfer order not found for " + id).build();
        }

        return Response.ok(order).build();
    }
}
