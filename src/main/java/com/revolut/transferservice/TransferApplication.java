package com.revolut.transferservice;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.revolut.transferservice.rest.controllers.HealthController;
import com.revolut.transferservice.rest.controllers.TransferOrderController;

public class TransferApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> clazzes = new HashSet<>();
        clazzes.add(HealthController.class);
        clazzes.add(TransferOrderController.class);
        return clazzes;
    }
}
