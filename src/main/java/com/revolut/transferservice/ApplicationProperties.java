package com.revolut.transferservice;

public class ApplicationProperties {

    public static final String APPLICATION_NAME = "Transfer-Service";

    public static final int APP_PORT = 8080;

    public static final String APP_HOST = "localhost";

    public static final int IO_THREADS = 2;

    public static final int WORKER_THREADS = 10;

    public static final String ROOT_PATH = "/";

    public static final String CONTEXT_PATH = "/api";

    public static final String DB_NAME = "jdbc:h2:mem://localhost/~/transfer-db";

    public static final String DB_USER = "sa";

    public static final String DB_PASSWORD = "";
}
