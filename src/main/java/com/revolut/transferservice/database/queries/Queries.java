package com.revolut.transferservice.database.queries;

import java.util.UUID;

import com.revolut.transferservice.rest.resources.TransferOrder;

public class Queries {

    public static String getTransferOrderTableQuery() {
        return "CREATE TABLE transfer_order(" + "to_id bigint auto_increment, " + "to_uuid UUID NOT NULL,"
                + "to_sender_id UUID NOT NULL, " + "to_recipient_account integer NOT NULL,"
                + "to_recipient_bank varchar(50) NOT NULL," + "to_amount integer NOT NULL,"
                + "to_scheduled_at varchar(25)," + "to_status enum('PENDING', 'PROCESSED') DEFAULT 'PENDING');";
    }

    public static String getTransferOrderCreationQuery(final TransferOrder transferOrder, final UUID uuid) {
        final String query = "INSERT INTO transfer_order (to_uuid,"
                + "to_sender_id, to_recipient_account, to_recipient_bank, to_amount) "
                + "VALUES ('%s', '%s', %d, '%s', %d);";

        return String.format(query, uuid, transferOrder.getSenderId(), transferOrder.getRecipientAccountNumber(),
                transferOrder.getRecipientBankCode(), transferOrder.getAmount());

    }

    public static String getTransferOrderRetrievalQuery(final UUID id) {
        return String.format("SELECT * FROM transfer_order WHERE to_uuid='%s';", id);
    }
}
