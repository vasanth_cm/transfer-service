package com.revolut.transferservice.database.pool;

import static com.revolut.transferservice.ApplicationProperties.DB_NAME;
import static com.revolut.transferservice.ApplicationProperties.DB_PASSWORD;
import static com.revolut.transferservice.ApplicationProperties.DB_USER;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DBConnectionPool {

    private static HikariConfig config = new HikariConfig();

    private static HikariDataSource dataSource;

    static {
        config.setJdbcUrl(DB_NAME);
        config.setUsername(DB_USER);
        config.setPassword(DB_PASSWORD);
        dataSource = new HikariDataSource(config);
    }

    private DBConnectionPool() { }

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
