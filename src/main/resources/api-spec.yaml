swagger: '2.0'
info:
  title: Transfer Service API
  description: |
    Provides endpoints for creating and retrieving transfer orders and registered users.
  version: "1.0.0"
host: transfer-service.com
basePath: /api
paths:
  /transfer-orders:
    post:
      summary: |
        Post a new transfer order. Returns order id as a response.
      consumes:
      - application/json
      produces:
      - application/json
      parameters:
      - name: payload
        in: body
        description: |
          JSON document representing transfer order.
        required: true
        schema:
          $ref: '#/definitions/TransferOrder'
      responses:
        '201':
          description: |
            The transfer order has been accepted successfully. Returns id of the transfer.
          schema:
            $ref: '#/definitions/TransferOrderResponse'
        '400':
          description: |
            For errors in user supplied data.
        '500':
          description: |
            Abnormal failure on server side.
  /transfer-orders/{transfer-order-id}:
    get:
      summary: |
        Get a transfer order that was previously created.
      produces:
      - application/json
      parameters:
      - name: transfer-order-id
        in: path
        required: true
        description: |
          The transfer order id for retrieving previously created transfer order
        type: string
        format: uuid
      responses:
        '200':
          description: |
            The referenced transfer order exists and the order is returned.
          schema:
            $ref: '#/definitions/TransferOrderResponse'
        '404':
          descriptiom: |
            No order was found for given id.
        '400':
          description: |
            For errors in user supplied data.
        '500':
          description: |
            Abnormal failure on server side.
  /users:
    post:
      summary: |
        Registers a new user interested in transferring money
      consumes:
      - application/json
      produces:
      - application/json
      parameters:
      - name: payload
        in: body
        description: |
          JSON document representing user request.
        required: true
        schema:
          $ref: '#/definitions/UserCreationRequest'
      responses:
        '201':
          description: |
            The user has been registered successfully. Returns id of the user.
          schema:
            $ref: '#/definitions/UserResponse'
        '400':
          description: |
            For errors in user supplied data.
        '500':
          description: |
            Abnormal failure on server side.
  /users/{user-id}:
    get:
      summary: |
        Retrieves a user with given id.
      produces:
      - application/json
      parameters:
      - name: user-id
        in: path
        description: |
          Id of the user
        required: true
        type: string
        format: uuid
      responses:
        '200':
          description: |
            The user exists and is returned.
          schema:
            $ref: '#/definitions/UserResponse'
        '404':
          descriptiom: |
            No user was found for given id.
        '400':
          description: |
            For errors in user supplied data.
        '500':
          description: |
            Abnormal failure on server side.
definitions:
  TransferOrder:
    type: object
    properties:
      sender_id:
        type: string
        format: uuid
      recipient_account_number:
        type: integer
        format: int32
      recipient_bank_code:
        type: string
      amount:
        description: |
          Amount to be transferred in cents.
        type: integer
        format: int64
        minimum: 100
        maximum: 1000000
      scheduled_at:
        type: date-time
  TransferOrderResponse:
    type: object
    properties:
      transfer_id:
        type: string
        format: uuid