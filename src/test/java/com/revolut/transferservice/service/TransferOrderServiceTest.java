package com.revolut.transferservice.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import static org.mockito.ArgumentMatchers.anyString;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.UUID;

import org.junit.Test;

import org.mockito.Mockito;

import com.revolut.transferservice.rest.resources.TransferOrder;

public class TransferOrderServiceTest {

    private Connection connection;

    private PreparedStatement statement;

    private TransferOrderService orderService;

    @Test
    public void createTransferOrder_returnsOrderUuid_whenSavedSuccessfully() throws Exception {
        connection = Mockito.mock(Connection.class);
        statement = Mockito.mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);

        orderService = new TransferOrderService(connection);

        final TransferOrder order = new TransferOrder(UUID.randomUUID(), 12345, "BC12345", 1000, null, "PENDING");

        final UUID orderId = orderService.createTransferOrder(order);

        assertNotNull("Should not be null", orderId);
        verify(statement).execute();
    }

    @Test
    public void createTransferOrder_returnsNullId_whenExceptionIsThrown() throws Exception {
        connection = Mockito.mock(Connection.class);
        statement = Mockito.mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        orderService = new TransferOrderService(connection);

        final TransferOrder order = new TransferOrder(UUID.randomUUID(), 12345, "BC12345", 1000, null, "PENDING");

        final UUID orderId = orderService.createTransferOrder(order);

        assertNull("Should be null", orderId);
    }
}
